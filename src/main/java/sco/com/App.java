/*
  Created by IntelliJ IDEA.
  User: rishabh.t
  Date: 22/08/20
  Time: 1:45 PM
*/

package sco.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
