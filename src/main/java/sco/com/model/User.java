/*
  Created by IntelliJ IDEA.
  User: rishabh.t
  Date: 22/08/20
  Time: 1:40 PM
*/

package sco.com.model;

public class User {

    private String name;

    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
