/*
  Created by IntelliJ IDEA.
  User: rishabh.t
  Date: 22/08/20
  Time: 1:40 PM
*/

package sco.com.model;

public class UserResponse {
    String content;

    public UserResponse() {
    }

    public String getContent() {
        return content;
    }

    public UserResponse(String content) {
        this.content = content;
    }
}
