/*
  Created by IntelliJ IDEA.
  User: rishabh.t
  Date: 22/08/20
  Time: 1:41 PM
*/

package sco.com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sco.com.service.socketioserivce.ISocketIOService;

@RestController
@RequestMapping("/api/socket")
public class UserController {

    @Autowired
    private ISocketIOService socketIOService;

    @PostMapping(value = "/pushMessageToUser")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public void pushMessageToUser(@RequestParam String userId, @RequestParam String msgContent) {
        socketIOService.pushMessageToUser(userId, msgContent);
    }

}